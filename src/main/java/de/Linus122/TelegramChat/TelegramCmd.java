package de.Linus122.TelegramChat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class TelegramCmd implements CommandExecutor {
	private final @NotNull Main main;

	public TelegramCmd(final @NotNull Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if (!cs.hasPermission("telegram.settoken")) {
			cs.sendMessage("§cYou don't have permissions to use this!");
			return true;
		}
		if (args.length == 0) {
			cs.sendMessage("§c/telegram [token]");
			return true;
		}
		main.getData().setToken(args[0]);
		main.save();

		main.getTelegram().connect(main.getData().getToken());
		if (main.getTelegram().isConnected()) {
			cs.sendMessage("§cSuccessfully connected to Telegram!");
			cs.sendMessage("§aAdd " + Objects.requireNonNull(main.getTelegram().getAuthJson())
					.getAsJsonObject("result").get("username").getAsString() + " to Telegram!");
		} else {
			cs.sendMessage("§cWrong token. Paste in the whole token!");
		}
		return true;
	}

}
