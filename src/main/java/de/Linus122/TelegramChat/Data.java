package de.Linus122.TelegramChat;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Data {
	private String token = "";
	// Player name // ChatID
	private Map<Long, UUID> linkedChats = new HashMap<>();
	// Player name // RandomInt
	private Map<String, UUID> linkCodes = new HashMap<>();
	public List<Long> ids = new ArrayList<Long>();
	private boolean firstUse = true;

	private Map<Long, UUID> groupLinkedChats = new HashMap<>();
	private Map<String, UUID> GroupLinkCodes = new HashMap<>();


	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Map<Long, UUID> getLinkedChats() {
		return linkedChats;
	}

	public Map<Long, UUID> getGroupLinkedChats() {
		return groupLinkedChats;
	}

	public void setLinkedChats(Map<Long, UUID> linkedChats) {
		this.linkedChats = linkedChats;
	}

	public Map<String, UUID> getLinkCodes() {
		return linkCodes;
	}

	public Map<String, UUID> getGroupLinkCodes() {
		return GroupLinkCodes;
	}

	public void setLinkCodes(Map<String, UUID> linkCodes) {
		this.linkCodes = linkCodes;
	}


	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public boolean isFirstUse() {
		return firstUse;
	}

	public void setFirstUse(boolean firstUse) {
		this.firstUse = firstUse;
	}

	public void addChatPlayerLink(long chatID, final @NotNull UUID player) {
		linkedChats.put(chatID, player);
	}

	public void addGroupChatPlayerLink(long userID, final @NotNull UUID player) {
		groupLinkedChats.put(userID, player);
	}


	public void addLinkCode(final @NotNull String code, final @NotNull UUID player) {
		linkCodes.put(code, player);
	}

	public UUID getUUIDFromLinkCode(final @NotNull String code) {
		return linkCodes.get(code);
	}

	public void removeLinkCode(final @NotNull String code) {
		linkCodes.remove(code);
	}

	public UUID getUUIDFromChatID(long chatID) {
		return linkedChats.get(chatID);
	}

	public UUID getUUIDFromUserID(long userID) {
		return groupLinkedChats.get(userID);
	}

}
