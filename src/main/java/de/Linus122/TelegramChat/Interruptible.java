package de.Linus122.TelegramChat;

@FunctionalInterface
public interface Interruptible {
    void interrupt();
}
