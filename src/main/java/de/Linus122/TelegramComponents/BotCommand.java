package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;

public class BotCommand {
    @SerializedName("command")
    private @NotNull String command;
    @SerializedName("description")
    private @NotNull String description;

    public BotCommand(final @NotNull String command, final @NotNull String description) {
        this.command = command;
        this.description = description;
    }

    public @NotNull String getCommand() {
        return command;
    }

    public void setCommand(final @NotNull String command) {
        this.command = command;
    }

    public @NotNull String getDescription() {
        return description;
    }

    public void setDescription(final @NotNull String description) {
        this.description = description;
    }
}
