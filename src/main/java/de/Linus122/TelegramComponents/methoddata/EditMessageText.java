package de.Linus122.TelegramComponents.methoddata;

import com.google.gson.annotations.SerializedName;
import de.Linus122.TelegramComponents.InlineKeyboardMarkup;
import de.Linus122.TelegramComponents.Message;
import de.Linus122.TelegramComponents.ParseMode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class EditMessageText {
    @SerializedName("chat_id")
    private long chatId;
    @SerializedName("message_id")
    private long messageId;
    @SerializedName("text")
    private @NotNull String text;
    @SerializedName("parse_mode")
    private @NotNull ParseMode parseMode;
    @SerializedName("reply_markup")
    private @Nullable InlineKeyboardMarkup replyMarkup;

    public EditMessageText(long chatId, long messageId, final @NotNull String text,
                           final @NotNull ParseMode parseMode) {
        this.chatId = chatId;
        this.messageId = messageId;
        this.text = text;
        this.parseMode = parseMode;
    }

    public static EditMessageText markdown(final @NotNull Message originalMessage, final @NotNull String text) {
        return new EditMessageText(originalMessage.getChat().getId(), originalMessage.getMessageId(),
                text, ParseMode.MARKDOWNV2);
    }

    public static EditMessageText markdown(long chatId, long messageId, final @NotNull String text) {
        return new EditMessageText(chatId, messageId, text, ParseMode.MARKDOWNV2);
    }

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public @NotNull String getText() {
        return text;
    }

    public void setText(final @NotNull String text) {
        this.text = text;
    }

    public @NotNull ParseMode getParseMode() {
        return parseMode;
    }

    public void setParseMode(final @NotNull ParseMode parseMode) {
        this.parseMode = parseMode;
    }

    public @Nullable InlineKeyboardMarkup getReplyMarkup() {
        return replyMarkup;
    }

    public void setReplyMarkup(final @Nullable InlineKeyboardMarkup replyMarkup) {
        this.replyMarkup = replyMarkup;
    }
}
