package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.Validate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class InlineKeyboardButton {
    @SerializedName("text")
    private @NotNull String text;
    @SerializedName("url")
    private @Nullable String url;
    @SerializedName("callback_data")
    private @Nullable String callbackData;

    public InlineKeyboardButton(final @NotNull String text) {
        this.text = text;
    }

    public InlineKeyboardButton(final @NotNull String text, final @Nullable String callbackData) {
        this.text = text;
        this.callbackData = callbackData;
    }

    public @NotNull String getText() {
        return text;
    }

    public void setText(final @NotNull String text) {
        Validate.inclusiveBetween(1, 64, text.length());
        this.text = text;
    }

    public @Nullable String getUrl() {
        return url;
    }

    public void setUrl(final @Nullable String url) {
        this.url = url;
    }

    public @Nullable String getCallbackData() {
        return callbackData;
    }

    public void setCallbackData(final @Nullable String callbackData) {
        if (callbackData != null) {
            Validate.inclusiveBetween(1, 64, callbackData.length());
        }
        this.callbackData = callbackData;
    }
}
