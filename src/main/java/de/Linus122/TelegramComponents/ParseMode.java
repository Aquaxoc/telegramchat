package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;

public enum ParseMode {
    @SerializedName("MarkdownV2")
    MARKDOWNV2
}
