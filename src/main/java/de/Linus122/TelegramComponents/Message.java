package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Message {
	@SerializedName("message_id")
	private long messageId;
	@SerializedName("from")
	private @Nullable User from;
	@SerializedName("date")
	private long date;
	@SerializedName("chat")
	private @NotNull Chat chat;
	@SerializedName("text")
	private @Nullable String text;
	@SerializedName("reply_markup")
	private @Nullable InlineKeyboardMarkup replyMarkup;

	public Message(long messageId, @Nullable User from, long date, @NotNull Chat chat, @Nullable String text) {
		this.messageId = messageId;
		this.from = from;
		this.date = date;
		this.chat = chat;
		this.text = text;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public @Nullable User getFrom() {
		return from;
	}

	public void setFrom(final @Nullable User from) {
		this.from = from;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public @NotNull Chat getChat() {
		return chat;
	}

	public void setChat(final @NotNull Chat chat) {
		this.chat = chat;
	}

	public @Nullable String getText() {
		return text;
	}

	public void setText(final @Nullable String text) {
		this.text = text;
	}

	public @Nullable InlineKeyboardMarkup getReplyMarkup() {
		return replyMarkup;
	}

	public void setReplyMarkup(final @Nullable InlineKeyboardMarkup replyMarkup) {
		this.replyMarkup = replyMarkup;
	}
}
