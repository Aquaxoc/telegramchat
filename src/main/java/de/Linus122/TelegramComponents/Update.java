package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.Nullable;

public class Update {
	@SerializedName("update_id")
	private long updateId;
	@SerializedName("message")
	private @Nullable Message message;
	@SerializedName("edited_message")
	private @Nullable Message editedMessage;
	@SerializedName("channel_post")
	private @Nullable Message channelPost;
	@SerializedName("edited_channel_post")
	private @Nullable Message editedChannelPost;
	@SerializedName("callback_query")
	private @Nullable CallbackQuery callbackQuery;

	public long getUpdateId() {
		return updateId;
	}

	public void setUpdateId(long updateId) {
		this.updateId = updateId;
	}

	public @Nullable Message getMessage() {
		return message;
	}

	public void setMessage(final @Nullable Message message) {
		this.message = message;
	}

	public @Nullable Message getEditedMessage() {
		return editedMessage;
	}

	public void setEditedMessage(final @Nullable Message editedMessage) {
		this.editedMessage = editedMessage;
	}

	public @Nullable Message getChannelPost() {
		return channelPost;
	}

	public void setChannelPost(final @Nullable Message channelPost) {
		this.channelPost = channelPost;
	}

	public @Nullable Message getEditedChannelPost() {
		return editedChannelPost;
	}

	public void setEditedChannelPost(final @Nullable Message editedChannelPost) {
		this.editedChannelPost = editedChannelPost;
	}

	public @Nullable CallbackQuery getCallbackQuery() {
		return callbackQuery;
	}

	public void setCallbackQuery(final @Nullable CallbackQuery callbackQuery) {
		this.callbackQuery = callbackQuery;
	}
}
