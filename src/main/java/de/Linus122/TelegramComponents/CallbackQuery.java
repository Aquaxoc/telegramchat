package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CallbackQuery {
    @SerializedName("id")
    private @NotNull String id;
    @SerializedName("from")
    private @NotNull User from;
    @SerializedName("message")
    private @Nullable Message message;
    @SerializedName("inline_message_id")
    private @Nullable String inlineMessageId;
    @SerializedName("chat_instance")
    private @Nullable String chatInstance;
    @SerializedName("data")
    private @Nullable String data;

    public CallbackQuery(final @NotNull String id, final @NotNull User from) {
        this.id = id;
        this.from = from;
    }

    public @NotNull String getId() {
        return id;
    }

    public void setId(final @NotNull String id) {
        this.id = id;
    }

    public @NotNull User getFrom() {
        return from;
    }

    public void setFrom(final @NotNull User from) {
        this.from = from;
    }

    public @Nullable Message getMessage() {
        return message;
    }

    public void setMessage(final @Nullable Message message) {
        this.message = message;
    }

    public @Nullable String getInlineMessageId() {
        return inlineMessageId;
    }

    public void setInlineMessageId(final @Nullable String inlineMessageId) {
        this.inlineMessageId = inlineMessageId;
    }

    public @Nullable String getChatInstance() {
        return chatInstance;
    }

    public void setChatInstance(final @Nullable String chatInstance) {
        this.chatInstance = chatInstance;
    }

    public @Nullable String getData() {
        return data;
    }

    public void setData(final @Nullable String data) {
        this.data = data;
    }
}
