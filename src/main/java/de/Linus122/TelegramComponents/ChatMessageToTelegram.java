package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ChatMessageToTelegram extends Cancellable {
	@SerializedName("text")
	private String text;
	@SerializedName("chat_id")
	private long chatId;
	@SerializedName("parse_mode")
	private @NotNull ParseMode parseMode = ParseMode.MARKDOWNV2;
	@SerializedName("reply_markup")
	private @Nullable InlineKeyboardMarkup replyMarkup;

	public static ChatMessageToTelegram markdown(final @NotNull String text) {
		final ChatMessageToTelegram self = new ChatMessageToTelegram();
		self.text = text;
		self.parseMode = ParseMode.MARKDOWNV2;
		return self;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getChatId() {
		return chatId;
	}

	public void setChatId(long chatId) {
		this.chatId = chatId;
	}

	public @NotNull ParseMode getParseMode() {
		return parseMode;
	}

	public void setParseMode(final @NotNull ParseMode parseMode) {
		this.parseMode = parseMode;
	}

	public @Nullable InlineKeyboardMarkup getReplyMarkup() {
		return replyMarkup;
	}

	public void setReplyMarkup(final @Nullable InlineKeyboardMarkup replyMarkup) {
		this.replyMarkup = replyMarkup;
	}
}
