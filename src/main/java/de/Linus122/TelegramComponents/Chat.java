package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;

public class Chat {
	@SerializedName("id")
	private long id;
	@SerializedName("type")
	private @NotNull ChatType type;

	public Chat(final long id, final @NotNull ChatType type) {
		this.id = id;
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public @NotNull ChatType getType() {
		return type;
	}

	public void setType(final @NotNull ChatType type) {
		this.type = type;
	}
	
	/**
	 * isPrivate
	 * @return true for private, false for group chats
	 */
	public boolean isPrivate(){
		return type == ChatType.PRIVATE;
	}
	

}
